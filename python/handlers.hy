(import [parser [*]])

(defn on-join [world data]
  (print "joined"))

(defn on-game-start [world data]
  (print "game started"))

(defn on-car-positions [world data]
    (save-car-positions world data))

(defn on-crash [world data]
  (print "someone crashed"))

(defn on-spawn [world data]
  (print "car spawned"))

(defn on-game-end [world data]
  (print "game end"))

(defn on-error [world data]
  (print "Did not finish"))

(defn on-else [world data]
  (print data))

(defn on-car [world data]
  (save-car-info world data))

(defn on-game-init [world data]
  (save-race-info world data)
  (print "racing on" (track-name world)))

(defn on-lap-finished [world data]
  (when
      (=
       (get (get data "car") "color")
       (own-color world))
    (print "I finished lap" (parse-lap-number data) "-" (parse-lap-time data))))

(defn on-finish [world data]
  (print "race done"))

(defn on-tournament-end [world data]
  (print "tournament ended")
  (stop-bot world))

(defn save-race-info [world data]
  (save-track-name world (parse-track-name data))
  (save-track-geometry world (parse-track-pieces data))
  (save-race-session world (parse-race-session data))
  (calculate-starting-strategy world data))

(defn save-car-positions [world data]
  (assoc world :old-positions (get world :car-positions))
  (assoc world :car-positions (parse-car-positions data world)))

