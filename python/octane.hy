(import [json])
(import [functools [partial]])
(import [parser [*]])
(import [handlers [*]])

(defn -send-message [socket message-type data]
  "send a message of a given type over socket"
  (let [[message (+ (.dumps json {"msgType" message-type "data" data}) "\n")]]
    (.sendall socket message)))

(defn quick-race [name key]
  "quick race"
  (send-message "join" {"name" name "key" key}))

(defn host [name key track-name car-count]
  "create race"
  (send-message "createRace" {"botId" {"name" name "key" key}
			 	      "carCount" car-count}))

(defn join [name key car-count]
  "join race"
  (send-message "joinRace" {"botId" {"name" name "key" key}
				    "carCount" car-count}))

(defn ping []
  "send ping to server"
  (send-message "ping" {}))

(defn throttle [throttle-amount]
  "send throttle message"
  (send-message "throttle" throttle-amount))

(defn curved? [piece]
  (in "angle" piece))

(defn decide-action [world]
  "decide what action should be taken next"
  :throttle)

(defn adjust-speed [world]
  "adjust speed"
  (throttle 0.65))


(defn adjust-lane [world]
  "adjust lane")

(defn -handle-message [message-handlers socket-file world]
  "receive and handle a message from server"
  (let [[line (.readline socket-file)]]
    (if line
      (let [[message (.loads json line)]
	    [message-type (get message "msgType")]
	    [data (get message "data")]]
	(if (in message-type message-handlers)
	  (let [[handler (get message-handlers message-type)]]
	    (handler data))
	  (let [[handler (get message-handlers "default")]]
	    (handler data))))
      (stop-bot world))))

(defn take-action [world]
  "decide what should be done next and act accordingly"
  (let [[action (decide-action world)]]
      (cond [(= action :throttle) (adjust-speed world)]
            [(= action :switch-lane) (adjust-lane world)])))

(defn message-loop [world]
  (while (bot-running? world)
    (handle-message world)
    (take-action world)))

(defn get-bot [socket bot-name bot-key &optional [action "quick race"]]
  (let [[world {:car-positions []
                :old-positions []}]
        [socket-file (.makefile socket)]
        [handlers {"yourCar" (partial on-car world)
			     "gameInit" (partial on-game-init world)
			     "gameStart" (partial on-game-start world)
			     "carPositions" (partial on-car-positions world)
			     "gameEnd" (partial on-game-end world)
			     "tournamentEnd" (partial on-tournament-end world)
			     "crash" (partial on-crash world)
			     "spawn" (partial on-spawn world)
			     "lapFinished" (partial on-lap-finished world)
			     "dnf" (partial on-error world)
			     "finish" (partial on-finish world)
			     "default" (partial on-else world)}]]
    (global handle-message)
    (global send-message)
    (setv handle-message (partial -handle-message handlers socket-file))
    (setv send-message (partial -send-message socket))
    (start-bot world)
    (cond [(= action "quick race") (quick-race bot-name bot-key)]
	  [(= action "host") (host bot-name bot-key "kemola" 2)]
	  [(= action "join") (join bot-name bot-key 2)])
    (partial message-loop world)))

