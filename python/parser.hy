(require hy.contrib.anaphoric)

(defreader t [expr] `(, ~@expr))

(defn start-bot [world]
  "start bot"
  (assoc world :running True))

(defn stop-bot [world]
  "stop bot"
  (assoc world :running False))

(defn bot-running? [world]
  "is bot running"
  (get world :running))

(defn own-color [world]
(when (in "color" (get world :car-info))
    (get (get world :car-info) "color")))

(defn parse-own-position [world data]
  (first (list-comp car-info [car-info data] 
		     (= (get (get car-info "id") "color") 
			(own-color world)))))

(defn parse-current-track-index [world data]
  (get (get (parse-own-position world data) "piecePosition") "pieceIndex"))

(defn save-car-info [world data]
  (assoc world :car-info data))

(defn parse-track-data [data]
  (get (get data "race") "track"))

(defn parse-track-name [data]
  (get (parse-track-data data) "name"))

(defn save-track-name [world track-name]
  (assoc world :track-name track-name))

(defn track-name [world]
  (get world :track-name))

(defn parse-track-pieces [data]
  {:pieces (get (parse-track-data data) "pieces")
   :lanes (get (parse-track-data data) "lanes")})

(defn save-track-geometry [world geometry]
  (assoc world :track-geometry geometry))

(defn track-info [world]
  (get world :track-geometry))

(defn track-geometry [world]
  (get (track-info world) :pieces))

(defn lane-geometry [world]
  (get (track-info world) :lanes))

(defn parse-race-session [data]
  (get (get data "race") "raceSession"))

(defn save-race-session [world session]
  (assoc world :race-session session))

(defn track-piece [world index]
  (get (track-geometry world) index))

(defn parse-current-track-piece [world data]
  (track-piece world (parse-current-track-index world data)))

(defn parse-next-track-piece [world data]
  (if (< (parse-current-track-index world data)
         (- (len (track-geometry world)) 1))
      (track-piece world (+ (parse-current-track-index world data) 1))
      (track-piece world 0)))

(defn current-track-piece [world]
  (current-location world (own-color world)))

;; (defn next-track-piece [world]
;;  (get world :next-track-piece))

(defn parse-lap-info [data]
  (get data "lapTime"))

(defn parse-lap-number [data]
  (get (parse-lap-info data) "lap"))

(defn parse-lap-time [data]
  (get (parse-lap-info data) "ticks"))

(defn parse-piece-position [car-node]
  (get car-node "piecePosition"))

(defn parse-lane [car-node]
  (get (parse-piece-position car-node) "lane"))

(defn previous-location [world color]
  (when (get world :old-positions)
    #t((get (get (get world :old-positions) color) :piece-index)
       (get (get (get world :old-positions) color) :in-piece-distance)
       (get (get (get world :old-positions) color) :start-lane))))

(defn current-location [world color]
    #t((get (get (get world :car-positions) color) :piece-index)
       (get (get (get world :car-positions) color) :in-piece-distance)
       (get (get (get world :car-positions) color) :start-lane)))

(defn piece-length [piece lanes lane-index]
  (if (in "length" piece)
    (get piece "length")
    (* (/ (get piece "angle") 360) 6.283 
       (+ (get piece "radius")
       (get (get lanes lane-index) "distanceFromCenter")))))

(defn current-speed [world color]
  (let [[position₀ (previous-location world color)]
	[position₁ (current-location world color)]]
    (when (and position₀ position₁)
      (if (= (first position₁) (first position₀))
	(- (second position₁) (second position₀))
	(let [[piece₀ (track-piece world (first position₀))]]
	  (+ (- (piece-length piece₀ (lane-geometry world) 1) (second position₀))
	     (second position₁)))))))

(defn parse-car-position [car-node world]
  #t((get (get car-node "id") "color")
       {:angle (get car-node "angle")
        :piece-index (get (parse-piece-position car-node) "pieceIndex")
        :in-piece-distance (get (parse-piece-position car-node) "inPieceDistance")
        :start-lane (get (parse-lane car-node) "startLaneIndex")
        :end-lane (get (parse-lane car-node) "endLaneIndex")}))

(defn parse-car-positions [data world]
  (dict (list-comp (parse-car-position car world) [car data])))

(defn guess-maximum-speed [section]
  (let [[new-section (.copy section)]]
    (if (in "angle" section)
      (assoc new-section :max-speed 6)
      (assoc new-section :max-speed 5))
  new-section))

(defn calculate-starting-strategy [world data]
  (save-track-geometry world 
    {:lanes (list (ap-map it (lane-geometry world)))
     :pieces (list (ap-map (guess-maximum-speed it) (track-geometry world)))}))

