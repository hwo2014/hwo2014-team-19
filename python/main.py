import json
import socket
import sys
import hy

from octane import get_bot

if __name__ == "__main__":
    if len(sys.argv) == 5:
        host, port, name, key = sys.argv[1:5]
        action = 'quick race'
    else:
        host, port, name, key, action = sys.argv[1:6]

    print("Connecting with parameters:")
    print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))       

    bot = get_bot(s, name, key, action)
    bot()

